# GitLab.com and CI Runners VPC Peering

## Summary

CI jobs are executed in VMs that are attached to VPC networks dedicated for these workloads. To clone/fetch Git data and upload artifacts, jobs connect to the public entrypoint of the website, GitLab.com. This means that the traffic goes through Public Internet, which is billed for both ingress and egress traffic costs. The fact that GCP the projects used for both CI GitLab.com production infrastructure are in the same GCP region doesn't give us any benefit.

In order to route Git traffic internally and safely from CI runners to GitLab.com, we decided to create a "middle layer": a dedicated VPC network (in the existing `gitlab-production` GCP project) with one internal load balancer (ILB) per availability zone. This new VPC will be peered with CI VPCs, thus giving CI jobs limited and scoped access to GitLab.com frontend LBs.

- [x] **Provide a high level summary of this new product feature. Explain how this change will benefit GitLab customers. Enumerate the customer use-cases.**
- [x] **What metrics, including business metrics, should be monitored to ensure will this feature launch will be a success?**: We will be monitoring Git traffic through the new front-end using the following dashboard https://dashboards.gitlab.net/d/frontend-git-haproxy/frontend-ci-gateway-git-utilisation-based-on-haproxy?orgId=1. The main business metric is bandwidth cost, specifically `Google Compute Engine: Network Egress via Carrier Peering Network - Americas` which is the bandwidth between CloudFlare and HAProxy. We will also monitor Cloudflare bandwidth, see https://gitlab.com/gitlab-org/gitlab/-/issues/348743 for details.

## Architecture

- [x] **Add architecture diagrams to this issue of feature components and how they interact with existing GitLab components. Include internal dependencies, ports, security policies, etc.**
- [x] **Describe each component of the new feature and enumerate what it does to support customer use cases.**
- [x] **For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?**
- [x] **If applicable, explain how this new feature will scale and any potential single points of failure in the design.**: There is no new scaling concern with this feature and no single points of failure.

The proposed architecture should look as follows:
![proposed-architecture.drawio](./img/proposed-architecture.drawio.png)

A new VPC named `ci-gateway` will be created in `gitlab-production` GCP project. Inside this VPC two new ILBs will be created, one for each zone within the `us-east1` region used by the CI runners, distributing traffic to new frontend HAProxy nodes in the zone they are in (i.e. ILB in `us-east1-c` only distributes traffic to HAProxy nodes in `us-east1-c`). These HAProxy nodes will have an extra network interface (NIC) in the `ci-gateway` VPC (in addition to the main NIC in `gprd` network), as ILBs can only distribute traffic to backends in the same VPC. Each ILB will have a DNS record named `git-us-east1-<zone>.ci-gateway.int.gprd.gitlab.net` pointing to the private IP of the ILB.

A new HAProxy frontend will added to HAProxy configurations that listens to a dedicate port (e.g. `8989`) and only accepts HTTPS Git traffic and traffic to a limited number of API endpoints required for GitLab Runner to operated. Any other requests are redirected to GitLab.com by means of 307 responses. The identification of HTTPS Git traffic is done by matching URLs against known patterns of HTTPS Git requests. Such patterns have been used in production for many years now for distinguishing regular web or API traffic from HTTPS Git one. The identification of the limited API endpoints traffic is done by matching URLs and methods against the endpoints patterns. Additionally the limited API endpoints traffic is limited to Runner Managers subnetwork by matching the source IP against the subnetwork's CIDR.

Finally, a VPC peering between `ci-gateway` and CI VPCs will be created, along with firewall rules that only allows traffic over port `8989` to the `ci-gateway` network. Existing firewall rules limiting communication between ephemeral VMs in the `10.0.0.0/8` network space will be updated to allow external traffic to the new ILB in the `ci-gateway` network.

With runners in each zone configured to use `url = git-us-east1-<zone>.ci-gateway.int.gprd.gitlab.net` and `clone_url = git-us-east1-<zone>.ci-gateway.int.gprd.gitlab.net`, communication between Runner and GitLab as well as the clone/fetch and the updates of submodules operations will be directed to the new ILBs. Artifacts upload/download requests will be redirected to GitLab.com to go through the Public Interned gateway. Runners communicating with an ILB within the same zone allows us not being charged for cross-zone traffic.

### Blast Radius

The blast radius for this change will be CI runners not being able to complete jobs if there is a problem with the new network configuration.
We are doing the following to help reduce the blast radius:
- Over-provisioning HAProxy so that we know we have enough compute capacity on the new HAProxy fleet
- Deploying gradually by starting with Private runners, and letting it sit there for over 24 hours
- As a runner needs to be configured with `url = git-us-east1-<zone>.ci-gateway.int.gprd.gitlab.net` and `clone_url = git-us-east1-<zone>.ci-gateway.int.gprd.gitlab.net` in order for the traffic to be routed internally, we can start by applying the configuration to a single runner and monitor the behavior of its jobs, then we can gradually roll it out to other runners.

## Operational Risk Assessment

- [x] **What are the potential scalability or performance issues that may result with this change?**: The main new scalability concern is that we are introducing a new HAProxy fleet as part of this configuration change. We will be slowly rolling out the configuration change and we are over-provisioning the HAProxy nodes. This will be evaluated post-configuration with https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/15248.
- [x] **List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the it will be impacted by a failure of that dependency.**: There are no new application dependencies introduced with this configuration change. The main dependency introduced is infra, two ILBs and a new HAProxy fleet. These new resources are deployed redundantly.
- [x] **Were there any features cut or compromises made to make the feature launch?**: None
- [x] **List the top three operational risks when this feature goes live.**: See below
- [x] **What are a few operational concerns that will not be present at launch, but may be a concern later?**: None
- [x] **Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?**: See below
- [x] **Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction.**: Not applicable, there is no new functionality being introduced by this configuration update.
- [x] **As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?**: The worse case scenario will be that jobs are unable to run due to one of the "Operational Risk" items listed below. See notes about how we think we have mitigated the risk for each item.

### Operational Risk

These are the main operational risks for this configuration change that we have done our best to mitigate:

1. Runners fail to process jobs due to misconfiguration: We are validating this change in Staging first with the same infrastructure and rolling it out slowly in Production.
1. Networking issue with the new peering config: The networking configuration in Staging will be the same as Production.
1. New HAProxy fleet is not sized properly for production fleet: This will be closely monitored during the slow rollout.

### Rollback

There are two options for rolling out this configuration change after it is deployed:
1. Reconfiguring the runners to use Cloudflare instead of the internal LBs
2. Configure the HAProxy servers to redirect all requests to Cloudflare

## Database

- [x] **If we use a database, is the data structure verified and vetted by the database team?**: Not applicable
- [x] **Do we have an approximate growth rate of the stored data (for capacity planning)?**: Not applicable
- [x] **Can we age data and delete data of a certain age?**: Not applicable

## Security and Compliance

- [x] **Were the [gitlab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?**: Not applicable
- [x] **If this feature requires new infrastructure, will it be updated regularly with OS updates?**: Not applicable
- [x] **Has effort been made to obscure or elide sensitive customer data in logging?**: Not applicable
- [x] **Is any potentially sensitive user-provided data persisted? If so is this data encrypted at rest?**: Not applicable
- [x] **Is the service subject to any regulatory/compliance standards? If so, detail which and provide details on applicable controls, management processes, additional monitoring, and mitigating factors.**: Not applicable

With this setup we are bypassing protections offered by our CDN provider, it is expected that CI jobs can reach `git-us-east1-<zone>.ci-gateway.int.gprd.gitlab.net` freely. Limited access to the API endpoints is required for Runner to properly communicate with GitLab and prepare valid Certificate Authority Chain for Git to use. However, we're only allowing job environments to have HTTPS Git traffic to go through, the rest will get 307 responses.

A dedicated cluster is used to serve this internal traffic, so the attack surface is reduced to a handful of nodes instead of the entirety of HAProxy nodes that serve all of GitLab.com traffic.

### Secrets

A wildcard certificate will created for the `git.ci-gateway.int.gprd.gitlab.net` domain name (i.e. a certificate for `*.ci-gateway.int.gprd.gitlab.net`). It's a wildcard one in anticipation to a situation where we create a similar setup for CI artifacts, so a domain name like `artifacts.ci-gateway.int.gprd.gitlab.net` can make use of the same certificate.

Both the private key and the certificates themselves will be stored in the `frontend-loadbalancer` GKMS vault. The key will also stored in 1Password.

## Performance

- [x] **Explain what validation was done following GitLab's [performance guidlines](https://docs.gitlab.com/ce/development/performance.html) please explain or link to the results below**: Not applicable
    * [Query Performer](https://docs.gitlab.com/ce/development/query_recorder.html)
    * [Sherlock](https://docs.gitlab.com/ce/development/profiling.html#sherlock)
    * [Request Profiling](https://docs.gitlab.com/ce/administration/monitoring/performance/request_profiling.html)
- [x] **Are there any potential performance impacts on the database when this feature is enabled at GitLab.com scale?**: Not applicable
- [x] **Are there any throttling limits imposed by this feature? If so how are they managed?**: There are no new throttles introduced by this configuration change
- [x] **If there are throttling limits, what is the customer experience of hitting a limit?**: There are no new limits introduced by this configuration change
- [x] **For all dependencies external and internal to the application, are there retry and back-off strategies for them?**: Not applicable
- [x] **Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?**: Not applicable

Although performance improvement is not the target of this setup, we expect latency to be reduced by some margin as the traffic would be handled internally. As traffic from runners will be concentrated on two zones (currently, runners exist in `us-east1-c` and `us-east1-d` only), there is a slight possibility of overloading the HAProxy nodes in these two zones. However, runners traffic roughly estimates to 16% of GitLab.com whole traffic, so we don't expect a noticeable load increase in HAProxy in zones `c` and `d`.

## Backup and Restore

- [x] **Outside of existing backups, are there any other customer data that needs to be backed up for this product feature?**: Not applicable
- [x] **Are backups monitored?**: Not applicable
- [x] **Was a restore from backup tested?**: Not applicable

## Monitoring and Alerts

- [x] **Is the service logging in JSON format and are logs forwarded to logstash?**: Not applicable
- [x] **Is the service reporting metrics to Prometheus?**: No change to metric reporting were needed for this configuration change.
- [x] **How is the end-to-end customer experience measured?**: We will use our existing SLOs for the runner service
- [x] **Do we have a target SLA in place for this service?**: Yes
- [x] **Do we know what the indicators (SLI) are that map to the target SLA?**: We will use existing CI runner SLOs
- [x] **Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?**: Yes
- [x] **Do we have troubleshooting runbooks linked to these alerts?**: Yes
- [x] **What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?**: When we breach SLOs this will follow our existing Incident process.
- [x] **do the oncall rotations responsible for this service have access to this service?**: Yes

A new [Grafana dashboard](https://dashboards.gitlab.net/d/frontend-git-haproxy/frontend-ci-gateway-git-utilisation-based-on-haproxy?orgId=1) for the new HAProxy frontend has been created.

As the domains `git-us-east1-<zone>.ci-gateway.int.gprd.gitlab.net` are only accessible through the `ci-gateway` network, we cannot directly monitor the certificate expiration the usual way (via blackbox-exporter probes). We can, however, leverage the fact that the certificate is a wildcard one. By registering a DNS name like `cert-check.ci-gateway.int.gprd.gitlab.net` pointing to the same private IP as `int.gprd.gitlab.net`, and by assigning the certificate to the `https` HAProxy frontend, we can then point blackbox-exporter to this new domain to get certificate expiration metrics.

## Responsibility

- [x] **Which individuals are the subject matter experts and know the most about this feature?**: @ahmadsherif @tmaczukin @jarv
- [x] **Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?**: SRE
- [x] **Is someone from the team who built the feature on call for the launch? If not, why not?**: @tmaczukin will be the primary contact from backend for this configuration rollout and will be available during working hours.

## Testing

- [x] **Describe the load test plan used for this feature. What breaking points were validated?**: See notes below
- [x] **For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.**: See notes below
- [x] **Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?**: Not applicable

Testing for this configuration change was first done in non-prod environments, PreProd and Staging.
Identical topology is configured in Staging and we have completed end-to-end testing in the Staging environment.
No specific load testing was completed, instead we will be doing a slow rollout in production while closely monitoring performance metrics as part of the Production change issue.
