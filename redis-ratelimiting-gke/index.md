# Redis Ratelimiting instance in GKE

## Summary

- [x] **Provide a high level summary of this new product
      feature. Explain how this change will benefit GitLab
      customers. Enumerate the customer use-cases.**

This is about decommissioning the 3 vms that are currently running for
the Redis-ratelimiting instance, and replacing them with 3 kubernetes
pods. This will reduce toil in managing these redis instances.

There should be no customer impact.

- [x] **What metrics, including business metrics, should be monitored
      to ensure will this feature launch will be a success?**

The already existing Dashboard for redis ratelimiting has been adapted
to support the nodes in Kubernetes: https://dashboards.gitlab.net/d/redis-ratelimiting-main/redis-ratelimiting-overview

## Architecture

- [x] **Add architecture diagrams to this issue of feature components
      and how they interact with existing GitLab components. Make sure
      to include the following: Internal dependencies, ports,
      encryption, protocols, security policies, etc.**

This feature is already in use, the architecture looks roughly like this:

```mermaid
sequenceDiagram
participant Client
participant RackAttack Middleware
participant Rails
participant Redis RateLimiting

Client ->> RackAttack Middleware: HTTP Request
  RackAttack Middleware -->> Redis RateLimiting: Increment request count
  Redis RateLimiting -->> RackAttack Middleware: Total request count
alt Within rate limit
  RackAttack Middleware ->> Rails: HTTP Request
  Rails ->> Client: 2xx Response
else Rate limited
  RackAttack Middleware ->> Client: 429 Response
end
```

We also have the [application rate
limiter](https://gitlab.com/gitlab-org/gitlab/blob/468212ec60ebc4d91b394e133d0555b8a57ed927/lib/gitlab/application_rate_limiter.rb#L17)
that uses this instance for specific operations, for example, creating
issues or notes. This happens further down the stack in the Rails
application itself, so the RackAttack rate limit has already been
enforced before hitting the Application Rate Limiter.

- [x] **Describe each component of the new feature and enumerate what
      it does to support customer use cases.**

This component only replaces the existing VMs we have, with a
Redis instance in Kubernetes.

- [x] **For each component and dependency, what is the blast radius of
      failures? Is there anything in the feature design that will
      reduce this risk?**

If Redis is unreachable, all requests to GitLab-Rails will fail. We
have seen this in QA: https://gitlab.com/gitlab-org/gitlab/-/issues/378152

RackAttack is currently not resilient to these kinds of failures. This
is currently the same as we have with the Redis-instances on VMs.

- [x] **If applicable, explain how this new feature will scale and any
      potential single points of failure in the design.**

Having the instance in Kubernetes should make it easier to scale up by
changing the nodepool. We run a single sentinel and redis per host.

We hope to be able to decrease the node size in the near future.

## Operational Risk Assessment

- [x] **What are the potential scalability or performance issues that
      may result with this change?**

The main risk is having the node underprovisioned. However, we
provisioned our redis GKE nodes to be c2-standard-8, in contrast with
our current redis VMs which are c2-standard-4, to accomodate for any
overhead from running on kubernetes.

Currently the [VMs have a maxmemory configuration of 8GB](https://gitlab.com/gitlab-com/gl-infra/chef-repo/blob/89102984ec8375d947f951f4980a607d50b95e93/roles/gprd-base-db-redis-server-ratelimiting.json#L47),
which we have reduced to 6GB based on usage. In the past 7 days we've
never
[seen usage above 1GB](https://thanos-query.ops.gitlab.net/graph?g0.expr=max_over_time(redis_memory_used_bytes%7Btype%3D%22redis-ratelimiting%22%2C%20env%3D%22gprd%22%7D%5B7d%5D)&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=&g1.tab=1&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D).

This should allow us to downgrade the machines to `c2-standard-4`
later.

- [x] **List the external and internal dependencies to the application
      (ex: redis, postgres, etc) for this feature and how the service
      will be impacted by a failure of that dependency.**

This instance is depended on by all our services hosting the
webservice: `api`, `git`, `web` & `websockets`. The instance becoming
unreachable would make these services unusable for our users.

- [x] **Were there any features cut or compromises made to make the
      feature launch?**

No

- [x] **List the top three operational risks when this feature goes
      live.**

- The instance not being accessible causing errors for users. This
  should be caught early by QA (as it has for staging). We intend to
  roll this out to cny, before proceeding.
- The instance being underprovisioned, making all requests slower for
  users. We have mitigated this by overprovisioning.
- There will be a split brain for ratelimiting temporarily during the rolllout, see details under [Testing](#testing) below.

- [x] **What are a few operational concerns that will not be present
      at launch, but may be a concern later?**

The same limitations and concerns apply as we have for Redis on
VMs. These are already included into tamland for forecasting:

https://gitlab-com.gitlab.io/gl-infra/tamland/redis.html#redis-ratelimiting-service-redis_primary_cpu-resource-saturation

We've added the kubernetes specific ones, but those aren't available
yet for gprd, as we don't have any instances:

https://thanos-query.ops.gitlab.net/graph?g0.expr=gitlab_component_saturation%3Aratio_quantile95_1h%7Btype%3D%22redis-ratelimiting%22%2C%20component%3D~%22kube.*%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D

- [x] **Can the new product feature be safely rolled back once it is
      live, can it be disabled using a feature flag?**

Yes, the configuration change can safely be rolled back until we
decommission the RateLimiting VMs.

Since the data is ephemeral, we don't care that any new data will not
be present in VMs. This effectively means that during a rollout and a
rollback the request-counter for a rate limit is reset.

- [x] **Document every way the customer will interact with this new
      feature and how customers will be impacted by a failure of each
      interaction.**

This is not directly used by customers, but all requests do
touch this instance. So all requests could be affected.

- [x] **As a thought experiment, think of worst-case failure scenarios
      for this product feature, how can the blast-radius of the
      failure be isolated?**

If the instance turns out to be unreachable, or becomes
unreachable while running. We need to roll back a configuration
change to fall back to using the VMs. We can limit the impact
during initial rollout by making the change to canary first.

## Database

- [x] **If we use a database, is the data structure verified and
      vetted by the database team?**

N/A

- [x] **Do we have an approximate growth rate of the stored data (for
      capacity planning)?**

Yes:
https://gitlab-com.gitlab.io/gl-infra/tamland/redis.html#redis-ratelimiting-service-redis_memory-resource-saturation

Because this is an LRU-evicting instance, we're unlikely to reach the
limit of amount of data stored

- [x] **Can we age data and delete data of a certain age?**

This instance is configured as an LRU. But we should never reach
maxmemory. All keys have a TTL set in terms of minutes.

## Security and Compliance

- **Are we adding any new resources of the following type? (If yes, please list them here or link to a place where they are listed)**
  - [X] **AWS Accounts/GCP Projects** No
  - [X] **New Subnets** No -- using current GKE subnets
  - [X] **VPC/Network Peering** No
  - [X] **DNS names** Yes, redis-ratelimiting-node-[0-2].redis-ratelimiting.$env.gke.gitlab.net, [managed by tanka](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/-/blob/ed030efdd3a92f904c4f115a1fcc6efbf2c2c626/lib/redis/instance.libsonnet#L139-143).
  - [X] **Entry-points exposed to the internet (Public IPs,
        Load-Balancers, Buckets, etc...)** No, load balancers are used but they are internal.
  - [ ] **Other (anything relevant that might be worth mention)**

- **Secure Software Development Life Cycle (SSDLC)**
    - [x] **Is the configuration following a security standard? (CIS is a good baseline for example)**
    - [x] **All cloud infrastructure resources are labeled according
          to the [Infrastructure Labels and
          Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/)
          guidelines** Yes
    - [x] **Were the [GitLab security development
          guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html)
          followed for this feature?** N/A
    - [x] **Do we have an automatic procedure to update the
          infrastructure (OS, container images, packages, etc...)**
          Yes, the chart will be automatically updated by renovate.
    - [x] **Do we use IaC (Terraform) for all the infrastructure
          related to this feature? If not, what kind of resources are
          not covered?** Yes

          - [ ] **Do we have secure static code analysis tools
           ([kics](https://github.com/Checkmarx/kics) or
           [checkov](https://github.com/bridgecrewio/checkov)) covering
           this feature's terraform?** Unsure?
  - **If there's a new terraform state:**
    - [x] **Where is to terraform state stored, and who has access to
          it?** N/A

  - [x] **Does this feature add secrets to the terraform state? If
        yes, can they be stored in a secrets manager?** Secrets are
        stored in Vault
  - **If we're creating new containers:** N/A
    - [X] **Are we using a distroless base image?** N/A
    - **Do we have security scanners covering these containers?** N/A
      - [X] **`kics` or `checkov` for Dockerfiles for example**
      - [X] **[GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities**

- **Identity and Access Management**
  -  [x] Are we adding any new forms of **Authentication** (New
         service-accounts, users/password for storage, OIDC, etc...)? No
  -  [x] **Does it follow the least privilege principle?** N/A

- **If we are adding any new Data Storage (Databases, buckets, etc...)**
  -  [x] **What kind of data is stored on each system? (secrets,
         customer data, audit, etc...)** Ephemeral rate limiting data:
         requests counts per IP/or user_id
  -  [x] **How is data rated according to our [data classification
         standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html)
         (customer data is RED)** Yellow
  -  [x] **Is data it encrypted at rest? (If the storage is provided
         by a GCP service, the answer is most likely yes)** Yes
  -  [x] **Do we have audit logs on data access?** No

- **Network security (encryption and ports should be clear in the architecture diagram above)**
  -  [X] **Firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)** Using standard GKE/k8s firewall configuration and network policies.
  -  [X] **Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)** No, but the application itself has its own protections.
  -  [X] **Is the service covered by a WAF (Web Application Firewall)** No

- **Logging & Audit**
  - [x] **Has effort been made to obscure or elide sensitive customer
        data in logging?** There is no logging of what goes in and out
        of Redis Ratelimiting

- **Compliance**
    - [x] **Is the service subject to any regulatory/compliance
          standards? If so, detail which and provide details on
          applicable controls, management processes, additional
          monitoring, and mitigating factors.** No


## Performance

- [ ] **Explain what validation was done following GitLab's [performance guidelines](https://docs.gitlab.com/ee/development/performance.html). Please explain which tools were used and link to the results below.**
- [x] **Are there any potential performance impacts on the database
      when this feature is enabled at GitLab.com scale?** No, this is
      not related to the database at all.
- [x] **Are there any throttling limits imposed by this feature? If so
      how are they managed?** This is about all throttling.
- [x] **If there are throttling limits, what is the customer
      experience of hitting a limit?** 429 responses
- [x] **For all dependencies external and internal to the application,
      are there retry and back-off strategies for them?** No
- [x] **Does the feature account for brief spikes in traffic, at least
      2x above the expected TPS?** Yes, it's supposed to limit these spikes.

## Backup and Restore

- [x] **Outside of existing backups, are there any other customer data
      that needs to be backed up for this product feature?** No the
      data is ephemeral.
- [ ] **Are backups monitored?** N/A
- [ ] **Was a restore from backup tested?** N/A

## Monitoring and Alerts

- [x] **Is the service logging in JSON format and are logs forwarded
      to logstash?** Yes: https://log.gprd.gitlab.net/goto/4e66fcd0-514c-11ed-b0ec-930003e0679c
- [x] **Is the service reporting metrics to Prometheus?** Yes:
      https://dashboards.gitlab.net/d/redis-ratelimiting-main/redis-ratelimiting-overview?orgId=1&var-PROMETHEUS_DS=Global
- [x] **How is the end-to-end customer experience measured?** Covered
      by our existing SLIs for that service and the SLIs for the
      web/api/git services.
- [x] **Do we have a target SLA in place for this service?** Yes, the
      same SLO as we used to have for the service on VMs.
- [x] **Do we know what the indicators (SLI) are that map to the
      target SLA?** Yes
- [x] **Do we have alerts that are triggered when the SLI's (and thus
      the SLA) are not met?** Yes
- [x] **Do we have troubleshooting runbooks linked to these alerts?**
      Yes:
      https://gitlab.com/gitlab-com/runbooks/blob/808310df1bbdf83f85aaf41338070638723b689f/docs/redis-ratelimiting/README.md#L1
- [x] **What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?** Standard for any GitLab.com outage.
- [x] **do the oncall rotations responsible for this service have
      access to this service?** Yes

## Responsibility

- [X] **Which individuals are the subject matter experts and know the
      most about this feature?**

SREs: @stejacks-gitlab, @msmiley, @alejandro,
@igorwwwwwwwwwwwwwwwwwwww

Backend: @reprazent

- [x] **Which team or set of individuals will take responsibility for
      the reliability of the feature once it is in production?**

The feature inside the application is un-owned. But our
infrastructure is relying on it, so we'll (Scalability) continue keeping an
eye as we have before.

- [X] **Is someone from the team who built the feature on call for the
launch? If not, why not?**

We'll be around when we roll this out to production

## Testing

- [ ] **Describe the load test plan used for this feature. What
      breaking points were validated?**
- [ ] **For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.**
- [ ] **Give a brief overview of what tests are run automatically in
      GitLab's CI/CD pipeline for this feature?**

Since rate-limiting applies to all requests, breaking configuration
changes would be noticed by QA (as they have in the past).

When we roll this out, we plan on doing it for canary first, and leave
enough time in between that and the full rollout to watch the
behaviour. This means that we'll have a split brain for our
ratelimiting temporarily: The counters for traffic going to canary
will be incremented in GKE-redis, while the others will still be in
the VM redis. This should be acceptable as Canary is only ~5% of
random traffic. When we proceed the rollout on the main stage, we'll
do one zone first, observe the load, and immediately continue to apply
the change to the other 2 zones. This limits the time we have the
split brain situation, while also limiting the blast radius in case
something goes south.

This is the second Redis instance we run in GKE, most things
applicable here are the same ones that have been validated for
[`redis-registry-cache`](../redis/registry-cache.md).
