# Registry service on GKE

The registry service uses the
[docker distribution](https://github.com/docker/distribution)
application and
allows GitLab.com users to store their own Docker images.

The service is currently installed and upgraded by installing the omnibus
package on a fleet of VMs. This set of documents documents the readiness
for migrating the registry service to Kubernetes. The service will utilize
the [Google Kubernetes Engine (GKE)](https://cloud.google.com/kubernetes-engine/),
a managed Kubernetes service offering by our current hosting provier.


The production readiness is divided into three documents that are targeted
for different audiences for review:

## Table of contents

  * [Architecture overview](#architecture-and-overview)
    * [Current configuration and how the registry works](#container-registry-architecture-using-vms-on-gitlabcom)
    * [Proposed configuration on GKE](#proposed-container-registry-architecture-on-gitlabcom-in-gke)
  * [Migration plan](#proposed-container-registry-architecture-on-gitlabcom-in-gke)
  * [Risk assessment](#risk-assessment-and-blast-radius-of-failures)
  * [Security considerations](#security-considerations)
  * [Registry application upgrade/rollback](#application-upgrade-and-rollback)
  * [Observability and Monitoring/Logging](#observability-and-monitoring)
  * [Testing](#testing)

## Architecture and Overview

The Registry service
uses GitLab.com as an authentication manager and Google Cloud Storage for storing
images.

* Clients interact with registry through the Docker daemon via the registry
  endpoint, `registry.gitlab.com`.
* Internal registry requests from GitLab.com use a short-lived(10 minutes) name-space
  limited token that is generated and signed with a private key.
* The Registry service **does not** connect directly to GitLab.com
* All state for the Registry service is on object storage, there is no locally
  persisted state.

Documentation References:

* Configure registry on separate nodes - https://docs.gitlab.com/omnibus/architecture/registry/#configuring-gitlab-and-registry-to-run-on-separate-nodes
* Registry token authentication - https://docs.docker.com/registry/spec/auth/token/
* Container registry overview - https://docs.gitlab.com/ee/user/project/container_registry.html

### Container Registry Architecture using VM's on GitLab.com

The Container Registry architecture on GitLab.com in August 2019 is shown below:
![Current Registry Architecture](./registry-current.png)

#### Push/Pull Operation by the Docker client

* ①  Attempt to push or pull operation is initiated by the Docker Client
* ①  Client makes a request to `registry.gitlab.com`
* ②  If the request is unauthorized, the registry service returns a `401
  unauthorize` and informs the client to retry the request to
  gitlab.com to obtain a Bearer token
* ①  Client retries the original request with the Bearer token embedded in the
  request's authorization header
* ①  registry.gitlab.com authorizes the request and begins the push or pull
  operation

#### List images on the gitlab.com project page

![List images](./registry-list-images.png)

* ③  A canary/web worker serves the request to the user's project page to
  list registry images
* ③  The canary/web worker makes a registry API call to list the tags for the corresponding
  registry repository `GET /v2/<user>/<project>/tags/list`


#### Delete an image from the gitlab.com project page

* ③  A canary/web worker serves the request to the user's project page to delete
  a registry tag
* ③  The canary/web worker makes a registry API call to delete the corresponding
  tag `DELETE /v2/<user>/<project>/manifests/sha256:<sha>"`
* ④  For the case when a project is deleted, the job that processes the
  repository deletion untags registry images

_Note: Images "deleted" in the registry service are untagged
but images themselves are not deleted, there is a tool to
[cleanup old images with garbage collection](https://docs.gitlab.com/omnibus/maintenance/README.html#container-registry-garbage-collection) which is not
currently run on GitLab.com because of the current number of images in the
registry. There is an
[ongoing effort](https://gitlab.com/groups/gitlab-org/-/epics/434) to manage
registry images on GitLab.com by setting retention periods_

#### Canary and Main stages on GitLab.com

There are two stages on GitLab.com that serve registry traffic.

* ⑤  Canary stage:  Traffic is directed to the canary stage of registry for
  a preset set of request paths, this includes requests for push/pull and API
  requests. The canary stage does not handle requests outside of specific
  request paths. This means the canary stage depends on the main stage to
  function for operations like `docker login`. In the case
  where the canary backend is unhealthy, traffic is automatically routed to the
  main stage.
* ⑥  Main stage:  The main stage handles all canary traffic that is not
  routed to canary. If the canary stage
  is unhealthy or marked down, the main stage will serve all requests.

---

### Proposed Container Registry Architecture on GitLab.com in GKE

![GKE Registry Architecture](./registry-gke-canary.png)


#### Configuration

All components that make up the GKE cluster and are put under source control in
the following locations:

* **[Terraform configuration](https://gitlab.com/gitlab-com/gitlab-com-infrastructure) ([ops mirror](https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure/)**
  * The following components are created in Terraform
    * GKE cluster
    * Cloud NAT for private cluster outbound access
    * Internal and public static IP addresses that are reserved for the cluster
    * Service account `k8s-workloads`

* **[Kubernetes and Helm configuration](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads) ([ops mirror](https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-com)**
  * There is a single project for every helm chart, for the registry service the project is
    [gitlab-com](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com)
    which is deployed to the `gitlab` namespace. The monitoring components
    (prometheus, alertmanager) are deployed to the `monitoring` namespace in the
    [monitoring project](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/monitoring)

For deployment the registry image is pulled directly from `docker.io/library/registry` this is an
external dependency, https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/7703 tracks
creating a local mirror.

_For more details about configuration see the howto for
[GitLab.com on Kubernetes](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/k8s-gitlab.md)_

#### Migration

The migration of the `registry.gitlab.com` to GKE will happen in two phases, one
to add GKE registry to canary stage and the second will be to the main stage.
these two phases are marked **P1** and **P2** on the diagram.

The two phases will be completed independently with enough time to closely
monitor the behavior of the cluster as it serves production traffic.

##### Phase 1: Attach the GKE cluster canary namespace to the canary backend

The first phase of the registry migration will be to modify the HAProxy
configuration so that traffic directed to canary will be directed to the
GKE canary namespace.

This will serve all registry requests for push/pull, listing and deleting image
tags for the canary request paths:

```
/v2/charts
/v2/gitlab-com
/v2/gitlab-org/gitlab-ee

```

_Because canary uses request based routing, all other requests will be
handled by the primary cluster_

##### Phase 2: Attach the Main GKE cluster to the main backend

The second phase of the registry migration will add the GKE registry
service address for the main stage to the server list for the primary
registry backend.

Utilizing server weights, it will be possible to move traffic from the
VM servers to the GKE cluster in a phased approach.

If the performance is acceptable on the new cluster, we will slowly move
more traffic to it and eventually turn down the legacy VMs.

## Risk Assessment and Blast radius of failures

### Phase 1

* In the case of a canary failure where no pods are healthy all traffic will
  be routed to the main stage automatically.
* In the case of a failure where pods are healthy during the Canary phase
  of deployment, push/pulls and listing the docker
  repositories from the project page will not function for the canary request
  paths. In this case, the canary stage can be disabled via ChatOps so
  that traffic is diverted to the main stage.

```
/chatops canary --disable --production
```

### Phase 2

* There are no changes to the blast radius of failures by moving the registry
  from VMs to GKE, if the GKE cluster is unavailable the following GitLab.com
  features will stop working
  * Pushing/pulling and `docker login` from clients
  * Registry queries from the project page including being able to list and
    remove images

## Security Considerations

### Service accounts

* A single service account named `k8s-workloads` is necessary for
  operations on the cluster that happen in CI.
  It has the following permissions:
    * Compute Network User
    * Kubernetes Engine Developer

_For more details about the service acount configuration see the
[project README](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/blob/master/README.md#service-account)_

### Network

```mermaid
graph TB;
  a(docker client) --> b(HAProxy:443);
  b --> c(registry:5000);
  a --> d(gitlab.com:443);
  d --> b
  c --> e(Google Object Storage)
  linkStyle 0 stroke-width:2px,fill:none,stroke:blue;
  linkStyle 1 stroke-width:2px,fill:none,stroke:red;
  linkStyle 2 stroke-width:2px,fill:none,stroke:blue;
  linkStyle 3 stroke-width:2px,fill:none,stroke:blue;
  linkStyle 4 stroke-width:2px,fill:none,stroke:blue;
```

#### Egress

* The registry service needs to communicate with Google Object Storage which
  requires outbound connections from the cluster.
* All egress connections to the internal `10.0.0.0/8` network and the GCP metadata
  service `169.254.169.254/32` are **by default disabled** via a networkpolicy
  on the pods running the service. This is configured in the project's
  [values.yaml](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/blob/adfa6915c9c577edf4353819ef6e89a9544d4345/values.yaml#L419).

#### Ingress

* Both Docker clients and GitLab.com connect with HTTPS to the registry on port
  443 using  the public endpoint (for production `registry.gitlab.com`).
* The registry service accepts TCP connections from HAProxy on port 5000 (highlighted in red
  on the diagram above), this internal connection is not encrypted.
  Encrypting the internal HAProxy to Registry network connection is tracked in
  https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/7569

### Secrets

_For up-to-date information on secrets including credential rotation see the [K8s operations runbook](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/k8s-gitlab-operations.md)_

#### Overview

* Kubelet stores the secret into a tmpfs so that the secret is not written to disk storage.
* Once the Pod that depends on the secret is deleted, kubelet will delete its local copy of the secret data as well.
* Communication between user to the apiserver, and from apiserver to the kubelets, is protected by SSL/TLS, see [Cluster Trust](https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-trust) for more information about how masters and nodes authenticate requests.
* By default, GKE encrypts customer content stored at rest, including secrets.
  For more information see [Application secrets in GKE](https://cloud.google.com/kubernetes-engine/docs/how-to/encrypting-secrets).
* Kubernetes offers envelope encryption of Secrets with a KMS provider, meaning that a local key, commonly called a data encryption key (DEK), is used to encrypt the Secrets.

References:

* Application secrets in GKE - https://cloud.google.com/kubernetes-engine/docs/how-to/encrypting-secrets
* Cluster Trust - https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-trust
* Secrets configuration in Kubernetes - https://kubernetes.io/docs/concepts/configuration/secret/


#### Types

* registry-httpsecret: Random data used to sign state that may be stored with
  the client to protect against tampering. This secret was first generated by
  [omnibus-gitlab](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/12.1.6+ce.0/files/gitlab-cookbooks/gitlab/libraries/registry.rb#L32)
  for the VM deployment, and copied into the GKE cluster.  A unique secret is
  used for every environment. For more information see the
  [registry configuration docs](https://docs.docker.com/registry/configuration/#http)
* internal_certificate: Contents of the certificate that GitLab uses to sign the tokens.
  The existing certificate from the existing environments are used and was originally generated by
  [gitlab-omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/10-3-stable/files/gitlab-cookbooks/registry/recipes/enable.rb#L60-66).
  For more information see the
  [registry token documentation](https://docs.docker.com/registry/configuration/#token).
  The validity of the certificate is until `May  9 00:38:46 2066 GMT`.
* registry_storage: Contains the GCS configuration and service account utilized to access the object storage bucket

All of the above are documented in the [secrets section of the HELM_README](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/blob/28e3acb94fce7d2bafaf32dec2c4e30193dba0d6/HELM_README.md#secret-for-gcs-configuration)

## Application Upgrade and Rollback

Application upgrade and rollback is handled through CI, there are two primary
upgrade cases:

* Helm chart version upgrades: This is set in `CHART_VERSION` in https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com
* Registry image upgrades: This is set in the environment files in https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com

Then either of these values are changed the following sequence happens for rollout:

* The corresponding version file is updated on a branch which triggers
  a pipeline on ops.gitlab.net to run a dry-run of `helm upgrade`
* The change is reviewed and merged, once merged to master a pipeline
  applies the change to the GKE cluster in sequence, first on the canary
  stage and then the main stage. The total pipeline time to initiate an
  update is ~5minutes for all stages.
* If there is an error that prevents a rollout the change is automatically
  rolled back. Reverting the versions on disk is not automated and is tracked
  by https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/7580

The diagram below illustrates the full sequence of actions
for a change from MR to production.

```mermaid
sequenceDiagram
    participant Administrator
    participant GitLab.com
    Administrator->>GitLab.com: Submits an MR for review
    loop CI
        GitLab.com->>GitLab.com: Syntax checks
    end
    GitLab.com->>ops.gitlab.net: branch mirrored to ops.gitlab.net
    loop CI
        ops.gitlab.net->>ops.gitlab.net: Dryrun apply on all envs
    end
    Administrator->>GitLab.com: MR merged to master
    loop CI
        GitLab.com->>GitLab.com: Syntax checks
    end
    GitLab.com->>ops.gitlab.net: master mirrored to ops.gitlab.net
    loop CI
        ops.gitlab.net->>ops.gitlab.net: Applied to non-production envs
    end
    loop CI
        ops.gitlab.net->>ops.gitlab.net: Applied to production canary
    end
    loop CI
        ops.gitlab.net->>ops.gitlab.net: Applied to production
    end
```


## Dogfooding

Wherever possible we try to leverage GitLab application features for managing
the deployments on Kubernetes. This includes:

* AutoDevops
* CI pipelines
* Package registry

The results of this investigation are documented in https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/7297#summary
and issues are tracked in the [Dogfooding Kubernetes features issue board](https://gitlab.com/groups/gitlab-org/-/boards/1284732?label_name[]=Delivery&label_name[]=Dogfooding&label_name[]=group%3A%3Aautodevops%20and%20kubernetes).

## Observability and Monitoring

Registry is monitored using prometheus and prometheus is installed in the
cluster in its own namespace using the
[prometheus operator helm chart](https://github.com/helm/charts/tree/master/stable/prometheus-operator)

There are multiple dashboards for monitoring both the GKE cluster itself and the Registry service

### GKE and Application Monitoring

_For up-to-date monitoring links see the [K8s operations runbook](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/k8s-gitlab-operations.md)_

Monitoring is installed and configured with [prometheus-operator](https://github.com/helm/charts/tree/master/stable/prometheus-operator) which is installed in the cluster
in a separate namespace.

### Alerts

_For up-to-date alert information see the [K8s operations runbook](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/k8s-gitlab-operations.md)_

In addition to prometheus, alertmanager is run in cluster, and has the same configuration as the
alertmanagers that are currently run in our production and ops environments.
The configuration is currently shared by a config file that is maintained
in object storage, encrypted with GKMS.

Rules are also shared between the current prometheus servers in production and
the cluster. To add new rules we use the same process as before, the CI pipeline
for runbooks https://gitlab.com/gitlab-com/runbooks will apply the updated
rules to the cluster

## Testing

Testing will be carried out in non-production environments for
basic validation including login, push, pull and seeing images
in the project registry page.

On canary we will be dogfooding the new GKE cluster for `gitlab-org`
and `gitlab-com` groups.

Following canary, using server weights the registry service will be
gradually take over the majority of traffic.

## Logging

_For up-to-date logging information see the [K8s operations runbook](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/k8s-gitlab-operations.md)_

All logging for the GKE cluster and all of its service is handled by
StackDriver with a log sink to pubsub. Like with the existing infrastructure,
a pubsubbeat is used to consume logs from pubsub and forward them to
elasticsearch. The container name can be used to filter logs for different
services running in the cluster, for example for production:

## Readiness review participants

1. [@cmiskell](https://gitlab.com/cmiskell): Also see https://gitlab.com/gitlab-com/gl-infra/readiness/issues/2
